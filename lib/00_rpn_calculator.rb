class RPNCalculator
  # TODO: your code goes here!
  def initialize
    @stack = []
  end

  def push(element)
    @stack << element
  end

  def value
    @stack.last
  end

  def plus
    do_math(:+)
  end

  def minus
    do_math(:-)
  end

  def times
    do_math(:*)
  end

  def divide
    do_math(:/)
  end

  def tokens(string)
    string_arr = string.delete(" ").chars
    tokens = string_arr.map do |el|
      if "+-*/".include?(el)
        el.to_sym
      else
        Integer(el)
      end
    end
    tokens
  end

  def evaluate(string)
    calc = RPNCalculator.new
    tokens = tokens(string)
    tokens.each do |el|
      if el.is_a?(Symbol)
        calc.do_math(el.to_sym)
      else
        calc.push(el)
      end
    end
    calc.value
  end

  def do_math(operator)
    if calculation_possible?
      number2 = @stack.pop
      number1 = @stack.pop

      case operator
      when :+
        @stack << number1 + number2
      when :-
        @stack << number1 - number2
      when :*
        @stack << number1 * number2
      when :/
        @stack << number1 / 1.0 / number2
      else
        @stack << number1
        @stack << number2
        raise "No such operation #{operator}"
      end

    else
      raise "calculator is empty"
    end
  end

  private

  def calculation_possible?
    if @stack.length < 2
      false
    else
      true
    end
  end

end
